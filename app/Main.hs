module Main where

import Types
import Game
import Parser
import Interface

import Graphics.Gloss.Interface.Pure.Game

-- =============== MAIN FUNCTION ==============================================
main :: IO()
main = do
    putStrLn "Welcome to Sudoku puzzle game" 
    interface  
    
-- graphics user interface ----------------------------------------------------                 
interface :: IO ()
interface = do
    w <- readWordd
    case gameState w of
        InProgress -> play display bgColor fps w 
                           drawWordd handleWordd updateWordd
        gs -> putStrLn "Incorrect input file"
    where
        windowSize = (floor winWidth, floor winHeight)
        windowOffset = (200, 200)
        display = InWindow "SUDOKU" windowSize windowOffset
        bgColor = white
        fps = 60
