module Types where

data Cell = Fixed Int | Filled Int | Empty deriving (Eq)

isFixed :: Cell -> Bool
isFixed (Fixed _) = True
isFixed _ = False

type Field = [[Cell]]

data MoveState = Ok | ErrFixed | ErrImpossible | 
                 Selected (Int, Int) | Hint (Int, Int)
                 

getSelected :: MoveState -> Maybe (Int, Int)
getSelected (Selected s) = Just s
getSelected (Hint     s) = Just s
getSelected _ = Nothing

isSelected :: MoveState -> Bool
isSelected ms =
    case getSelected ms of
        Nothing -> False
        Just _  -> True

data GameState
  = Finished
  | InProgress
  | Result
  | Error         -- ^ ???
  deriving Eq

data Wordd = Wordd
    { field     :: Field      -- ^ sudoku grid
    , moveState :: MoveState  -- ^ last move state (results)
    , gameState :: GameState  -- ^ global game state
    }
