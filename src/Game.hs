module Game where

import Types
        
isIn :: [Int] -> Int -> Bool
isIn [] _ = False
isIn (x:xs) y | y == x = True
              | otherwise = (isIn xs y)    

centerInd :: Int -> Int
centerInd i = 3 * (div i 3) + 1  

makeMove :: Wordd -> Int -> Int -> Int -> Wordd               
makeMove w row col num = w'
    where
        f'  = fillCell (field w) row col num
        w'  = w { field = f'
                , moveState = Selected (row, col)
                , gameState = getGameState f'
                }

getGameState :: Field -> GameState
getGameState f =
    if not isFin 
        then InProgress
        else if isCorrFld 
            then Finished
            else Error
    where 
        isFin = checkFinished f
        isCorrFld = checkField f  
        
clearCell :: Wordd -> Int -> Int -> Wordd
clearCell w row col =
    if isFixed c 
        then w { moveState = ErrFixed }
    else w { field = setCell Empty row col f
           , moveState = Selected (row, col)
           , gameState = InProgress }
    where
        f = field w
        c = getCell f row col

fillCell :: Field -> Int -> Int -> Int -> Field
fillCell f row col num = setCell (Filled num) row col f    
    
setCell :: Cell -> Int -> Int -> Field -> Field
setCell c row col f = left ++ (top ++ c : bottom) : right
    where
        (left, mid : right) = splitAt row f
        (top, _ : bottom) = splitAt col mid

getCell :: Field -> Int -> Int -> Cell
getCell f row col = (f !! row) !! col 
    
checkLine :: [Cell] -> Bool
checkLine l = checkLine_r l []  

checkLine_r :: [Cell] -> [Int] -> Bool
checkLine_r [] _ = True
checkLine_r (Empty : xs) tmp = checkLine_r xs tmp
checkLine_r ((Fixed x) : xs) tmp  | isIn tmp x = False
                                  | otherwise = checkLine_r xs (x : tmp)
checkLine_r ((Filled x) : xs) tmp | isIn tmp x = False
                                  | otherwise = checkLine_r xs (x : tmp)               

checkRow :: Field -> Int -> Bool
checkRow f i = checkLine (f !! i)     

checkCol :: Field -> Int -> Bool
checkCol f j = checkLine (getCol f j)          

getCol :: Field -> Int -> [Cell]
getCol [] _ = []
getCol (x:xs) i = (x !! i) : (getCol xs i)

checkSqr :: Field -> Int -> Int -> Bool
checkSqr f row col = checkLine (getSqr f (centerInd row) (centerInd col))

getSqr :: Field -> Int -> Int -> [Cell]
getSqr f row col = 
    [f !! i !! j 
        | i <- [row - 1, row, row + 1],
          j <- [col - 1, col, col + 1]]               

checkFill :: Field -> Int -> Int -> Bool
checkFill f row col = checkRow f row && 
                      checkCol f col &&
                      checkSqr f row col  
    
checkField :: Field -> Bool 
checkField f = checkAllRows f && checkAllCols f && checkAllSqrs f

checkAllRows :: Field -> Bool
checkAllRows [] = True
checkAllRows (x : xs) = (checkLine x) && (checkAllRows xs)

checkAllCols :: Field -> Bool
checkAllCols f = checkAllCols_r f 0

checkAllCols_r :: Field -> Int -> Bool
checkAllCols_r f i | i == 8 = checkCol f i
                   | otherwise = checkCol f i && checkAllCols_r f (i + 1)

checkAllSqrs :: Field -> Bool
checkAllSqrs f = checkSqr f 1 1 && checkSqr f 1 4 && checkSqr f 1 7 &&
                 checkSqr f 4 1 && checkSqr f 4 4 && checkSqr f 4 7 &&
                 checkSqr f 7 1 && checkSqr f 7 4 && checkSqr f 7 7
                     
checkFinished :: Field -> Bool
checkFinished [] = True
checkFinished (x : xs) = (checkRowFinished x) && (checkFinished xs)      

checkRowFinished :: [Cell] -> Bool
checkRowFinished = all (/= Empty)

isPossible :: Field -> Int -> Int -> Int -> Bool
isPossible f row col num = checkFill (fillCell f row col num) row col

