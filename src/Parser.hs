module Parser where

import Types
import Game

import Data.Functor
import Data.Char

import System.IO

readWordd :: IO Wordd
readWordd = do
    putStr "Enter file name: "
    hFlush stdout
    file <- getLine
    readFromFile file

readFromFile :: FilePath -> IO Wordd
readFromFile file = do
    strList <- lines <$> readFile file
    let f = createField strList
    if checkField f && not (checkFinished f) then     
        return (Wordd f Ok InProgress)
    else
        return (Wordd [] Ok Error)

-- transform file contents into field representation --------------------------
createField :: [String] -> Field
createField [] = []
createField (x : xs) = (parseString x) : (createField xs)

parseString :: String -> [Cell]
parseString [] = []
parseString ('0' : xs) = Empty : parseString xs
parseString (x : xs) 
    | isDigit x = Fixed (ord x - ord '0') : parseString xs
    | otherwise = parseString xs 
