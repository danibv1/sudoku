module Interface where

import Types
import Game

import Data.Char
import Data.Monoid 

import Graphics.Gloss.Interface.Pure.Game

winHeight :: Float -- game window height
winHeight = 540

winWidth :: Float -- game window width
winWidth = (winHeight + 3 * cellSize) + 100 -- the second item stands for the
                                            -- interval between field 
                                            -- and numberpad

tlppX :: Float -- top left pixel position (X)                               
tlppX = - winWidth / 2 

tlppY :: Float -- top left pixel position (Y)
tlppY = winHeight / 2  

trppX :: Float -- top right pixel position (X)
trppX = winWidth / 2   

trppY :: Float -- top right pixel position (Y)
trppY = winHeight / 2  

blppX :: Float -- bottom left pixel position (X)
blppX = tlppX      

blppY :: Float -- bottom left pixel position (Y)      
blppY = - tlppY          

brppX :: Float -- bottom right pixel position (X)
brppX = trppX    

brppY :: Float -- bottom right pixel position (Y)        
brppY = - trppY          

cellSize :: Float -- size of a cell 
cellSize = winHeight / 9  

tlcpX :: Float -- top left cell position (X)
tlcpX = (cellSize / 2) - (winWidth / 2)
 
tlcpY :: Float -- top left cell position (Y)
tlcpY = - (cellSize / 2) + (winHeight / 2)

trnpX :: Float -- top right number position (X)
trnpX = (winWidth / 2) - (cellSize / 2)
 
trnpY :: Float -- top right number position (Y)
trnpY = (winHeight / 2) - (cellSize / 2) 

digScl :: Float -- digits scale to fit the cells 
digScl = (cellSize / 50) * 0.3 

cellPosX :: Int -> Float -- cell X coordinate by its column index
cellPosX col = tlcpX + cellSize * (fromIntegral col)

cellPosY :: Int -> Float -- cell Y coordinate by its row index
cellPosY row = tlcpY - cellSize * (fromIntegral row)

numPosX :: Float -> Float -- number X coordinate according to cell X coordinate
numPosX posX = posX - (cellSize / 4)

numPosY :: Float -> Float -- number Y coordinate according to cell Y coordinate
numPosY posY = posY - (cellSize / 4)

rowByY :: Float -> Int -- cell row index by Y coordinate
rowByY y = 8 - floor ( (y + winHeight / 2) / cellSize )

colByX :: Float -> Int -- cell column index by X coordinate
colByX x 
    | x <= tlppX + 9 * cellSize = floor ( (x + winWidth / 2) / cellSize )
    | otherwise                 = (-1)
         
numByXY :: Float -> Float -> Int -- chosen number on the numberpad     
numByXY x y = ncol + (3 * nrow) + 1
    where
        nrow = 2 - floor ( (y - winHeight / 2 + 3 * cellSize) / cellSize ) 
        ncol = floor ( (x - winWidth / 2 + 3 * cellSize) / cellSize )
    
drawWordd :: Wordd -> Picture
drawWordd (Wordd f ms gs) =
    case gs of
        Finished -> drawFinish
        Result   -> drawFinish
        _ -> drawField f ms 
             <> drawNumberpad 

drawFinish :: Picture
drawFinish = translate (-5 * cellSize / 2) (2 * cellSize) 
                 (scale (1.5 * digScl) (1.5 * digScl) 
                     (text "YOU WIN"))

-- selected cell overlay ------------------------------------------------------                                      
drawSelectedCell :: Int -> Int -> Picture
drawSelectedCell srow scol = 
    color green (drawCell Empty (cellPosX scol) (cellPosY srow)
                          cellSize cellSize)  
                                          
drawField :: Field -> MoveState -> Picture
drawField f (Selected (srow, scol)) = drawAllCells f 
                                   <> drawBoldLines
                                   <> drawGrid
                                   <> drawSelectedCell srow scol
                                   
drawField f _ = drawAllCells f 
             <> drawBoldLines
             <> drawGrid
             
drawGrid :: Picture
drawGrid = pictures [(drawCell Empty (cellPosX col) (cellPosY row) 
                               cellSize cellSize) 
                        | row <- [0,1 ..8],  
                          col <- [0,1 ..8]] 
                                
drawBoldLines :: Picture
drawBoldLines = pictures
    [drawCell Empty (cellPosX col) (cellPosY row) 
                    (3 * cellSize - 2) (3 * cellSize - 2)            
    | col <- [1,4,7],
      row <- [1,4,7]]    
              
drawCell :: Cell -> Float -> Float -> Float -> Float -> Picture
drawCell Empty posX posY sizeX sizeY = 
    translate posX posY (rectangleWire sizeX sizeY)
        
drawCell (Filled n) posX posY sizeX sizeY = 
    pictures [translate posX posY (rectangleWire sizeX sizeY),
              translate (numPosX posX) (numPosY posY) 
                  (scale digScl digScl (text (show n)))]   
                  
drawCell (Fixed n) posX posY sizeX sizeY = 
    pictures [color clr (translate posX posY (rectangleSolid sizeX sizeY)),
              translate (numPosX posX) (numPosY posY) 
                  (scale digScl digScl (text (show n)))] 
    where 
        clr = dark white    
                  
drawAllCells :: Field -> Picture
drawAllCells f = pictures [(drawCell (getCell f row col)
                                     (cellPosX col) (cellPosY row) 
                                     cellSize cellSize) 
                               | row <- [0,1 ..8],  
                                 col <- [0,1 ..8]] 
                                 
drawNumberpad :: Picture
drawNumberpad =  pictures
    [ drawCell (Filled n) offsetX offsetY cellSize cellSize
    | (n, (i, j)) <- numbers
    , let offsetX = trnpX - i * cellSize
    , let offsetY = trnpY - j * cellSize ]
    where
        numbers = zip [1..9] coords
        coords  = [ (i, j) | j <- [0..2], i <- [2, 1, 0] ]
                
-- =============== EVENTS HANDLER =============================================

-- main handler function ------------------------------------------------------
-- defines what action to do according to mouse click coordinates -------------    
handleWordd :: Event -> Wordd -> Wordd
handleWordd _ w@Wordd{ gameState = Finished } = w

handleWordd (EventKey (MouseButton LeftButton) Down _ (x, y)) w 
    = handleMouseLeft w x y
handleWordd (EventKey (MouseButton RightButton) Down _ (x, y)) w  
    = handleMouseRight w x y
  

handleWordd (EventKey (SpecialKey KeySpace) Down _ _) 
            w@Wordd{ moveState = Selected (row, col) } 
    = clearCell w row col
handleWordd (EventKey (SpecialKey KeySpace) Down _ _) 
            w@Wordd{ moveState = Hint (row, col) } 
    = clearCell w row col
  
handleWordd (EventKey (Char c) Down _ _) w =
    case moveState w of
        Selected _ -> handleChar c w
        Hint     _ -> handleChar c w
        _ -> w

handleWordd (EventKey (SpecialKey KeyRight) Down _ _) w  
    = handleRight w
handleWordd (EventKey (SpecialKey KeyLeft) Down _ _) w  
    = handleLeft w
handleWordd (EventKey (SpecialKey KeyUp) Down _ _) w 
    = handleUp w
handleWordd (EventKey (SpecialKey KeyDown) Down _ _) w
    = handleDown w
              
handleWordd _ w = w    

-- handlers for mouse buttons -------------------------------------------------
-------------------------------------------------------------------------------
handleMouseLeft :: Wordd -> Float -> Float -> Wordd
handleMouseLeft w x y =     
    if fieldHit x y    
        then handleSelect w x y
        else if numpadHit x y 
                 then handleMove w x y
                 else w

-------------------------------------------------------------------------------
handleMouseRight :: Wordd -> Float -> Float -> Wordd
handleMouseRight w x y = 
    if fieldHit x y    
        then handleHint w x y
        else w                  

-------------------------------------------------------------------------------        
-- handler for common buttons -------------------------------------------------                 
handleChar :: Char -> Wordd -> Wordd
handleChar '0' w = w
handleChar c w =
    case getSelected (moveState w) of
        Nothing -> w
        Just (row, col) ->
            if isDigit c
                then makeMove w { moveState = Selected (row, col) } 
                              row col (ord c - ord '0')
            else if c == 'h' 
                     then w { moveState = Hint (row,col) }
                     else w

handleDir :: (Field -> Int -> Int -> (Int, Int)) -> Wordd -> Wordd
handleDir closest w =
    case getSelected (moveState w) of
        Nothing -> w { moveState = Selected (closestNotFixed (field w) 0 0) }
        Just (row, col) ->
            let (row', col') = closest (field w) row col
            in if check row' col'
                   then w { moveState = Selected (row', col') }
                   else w { moveState = Selected (row, col) }
    where
        check row col = row >= 0 && row < 9 && col >= 0 && col < 9

closestNotFixed :: Field -> Int -> Int -> (Int, Int)
closestNotFixed f row col 
    | (col == 8) && (isFixed(getCell f row col))= closestNotFixed f (row+1) 0
    | (isFixed (getCell f row col))             = closestNotFixed f row (col+1)   
    | otherwise                                 = (row, col)
    
handleRight :: Wordd -> Wordd
handleRight = handleDir closestRight

closestRight :: Field -> Int -> Int -> (Int, Int)
closestRight f row col | ((col == 8) && (isFixed (getCell f row col)))
                             = (row,9)
                       | ((col <= 7) && (isFixed (getCell f row (col+1))))
                             = closestRight f row (col+1) 
                       | otherwise = (row, col+1)

handleLeft :: Wordd -> Wordd
handleLeft = handleDir closestLeft

closestLeft :: Field -> Int -> Int -> (Int, Int)
closestLeft f row col | ((col == 0) && (isFixed (getCell f row col)))
                             = (row,-1)
                       | ((col >= 1) && (isFixed (getCell f row (col-1))))
                             = closestLeft f row (col-1) 
                       | otherwise = (row, col-1)          
                       
handleUp :: Wordd -> Wordd
handleUp = handleDir closestUp

closestUp :: Field -> Int -> Int -> (Int, Int)
closestUp f row col    | ((row == 0) && (isFixed (getCell f row col)))
                             = (-1,col)
                       | ((row >= 1) && (isFixed (getCell f (row-1) (col))))
                             = closestUp f (row-1) (col) 
                       | otherwise = (row-1, col)    
                       
handleDown :: Wordd -> Wordd
handleDown = handleDir closestDown

closestDown :: Field -> Int -> Int -> (Int, Int)
closestDown f row col  | row == 8 && isFixed (getCell f row col)
                             = (9,col)
                       | row <= 7 && isFixed (getCell f (row+1) col)
                             = closestDown f (row+1) col
                       | otherwise = (row+1, col) 

handleSelect :: Wordd -> Float -> Float -> Wordd
handleSelect w x y = do 
    if isFixed c
      then w
      else w { moveState = Selected (row, col) }
    where
        row = rowByY y 
        col = colByX x
        c = getCell (field w) row col 
        
handleMove :: Wordd -> Float -> Float -> Wordd
handleMove w x y =
    case moveState w of
        Selected (row, col) -> handleSelectedCell row col    
        Hint     (row, col) -> handleSelectedCell row col
        _ -> w
    where
        handleSelectedCell row col = do
            let num = numByXY x y
            if (num <= 9) 
                then makeMove w { moveState = Selected (row, col) } row col num   
                else clearCell w { moveState = Selected (row, col) } row col

handleHint :: Wordd -> Float -> Float -> Wordd
handleHint w x y = do  
    if isFixed c 
        then w
        else w { moveState = Hint (row, col) }
    where 
        row = rowByY y  
        col = colByX x 
        c = getCell (field w) row col 
        
-------------------------------------------------------------------------------
fieldHit :: Float -> Float -> Bool
fieldHit x y = (x <= tlppX + 9 * cellSize)  && (y >= tlppY - 9 * cellSize)  &&
               (x >= tlppX)                 && (y <= tlppY) 

numpadHit :: Float -> Float -> Bool 
numpadHit x y = (x >= trppX - 3 * cellSize) && (y >= trppY - 4 * cellSize)  &&
                (x <= trppX)                && (y <= trppY)                                

-- increases total game time with each frap -----------------------------------
updateWordd :: Float -> Wordd -> Wordd
updateWordd dt w = 
    case gameState w of
       _           -> w
